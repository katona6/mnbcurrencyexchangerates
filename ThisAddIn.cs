﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Excel;
using MNB_adatletoltes.MNBCurrencyServiceSoap;
using System.Runtime.InteropServices;
using System.Collections;

namespace MNB_adatletoltes
{
    public partial class ThisAddIn
    {
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            this.Application.WorkbookBeforeSave += new Microsoft.Office.Interop.Excel.AppEvents_WorkbookBeforeSaveEventHandler(MNB_ExchangeRates_VSTO_Excel_AddIn);
        }
        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }
        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }

        #endregion

        //METHODS THIS ADD-IN USES
        /// <summary>
        /// Returns a String with MNB Query Results
        /// Parameters needed in the following string formats: 
        /// </summary>
        /// <param name="startDate">- startDate     : "2015-01-01",</param>
        /// <param name="endDate">- endDate       : "2015-12-31",</param>
        /// <param name="currencyNames">- currencyNames : "EUR, HUF"</param>
        public String GetExchangeRates(string startDate, string endDate, string currencyNames)
        {
            /*_________________________________MNB_QUERY_____________________________________________*/
            //Service Client Object initialize
            MNBArfolyamServiceSoapClient client = new MNBArfolyamServiceSoapClient();
            //Create Request Body in order to set Query Properties
            GetExchangeRatesRequestBody GERRB = new GetExchangeRatesRequestBody();
            //Client connection fail timeout in ms
            //client.Timeout = 100;
            bool repeat_again = true;
            int repeat_times = 100;
            int test = 0;
            string OLEerr = "Unknown Error happened... \nIsmeretlen hiba történt...";

            //Create a StringBuilder object to retrieve information into
            StringBuilder query_result = new StringBuilder();

            while (repeat_again)
            {
                test += 1;

                try
                {
                    //Set query properties
                    GERRB.startDate = startDate;
                    GERRB.endDate = endDate;
                    GERRB.currencyNames = currencyNames;
                    
                    GetExchangeRatesResponseBody queryResultBody = client.GetExchangeRates(GERRB);
                    
                    
                    repeat_again = false;
                    if (queryResultBody.GetExchangeRatesResult.Length != 0)
                    {
                        query_result.AppendLine("\n");
                        query_result.AppendLine(queryResultBody.GetExchangeRatesResult);

                    }
                }
                catch (InvalidOperationException ex)
                {
                    repeat_again = true;
                    OLEerr = ex.GetType().FullName + ex.Message;
                }

                if (test >= repeat_times)
                {
                    System.Console.WriteLine(OLEerr);
                    repeat_again = false;
                }

            }
            //Close connection on query finish & return with the Result
            client.Close();
            return query_result.ToString();
        }

        //MAIN ADD-IN Engine
        public void MNB_ExchangeRates_VSTO_Excel_AddIn(Microsoft.Office.Interop.Excel.Workbook Wb, bool SaveAsUI, ref bool Cancel)
        {
            /*_______INSERT_QUERY_RESULTS_INTO_DATA_STRUCTURES___&&___PUT_VALUES_TO_EXCEL_CELLS__________*/
            Excel.Worksheet activeWorksheet = ((Excel.Worksheet)Application.ActiveSheet);
            Excel.Range firstRow = activeWorksheet.get_Range("A1");
            firstRow.EntireRow.Insert(Excel.XlInsertShiftDirection.xlShiftDown);
            //AUTO-FIT CELLS WIDTH
            activeWorksheet.StandardWidth = 10;
            //PUT MAIN TITLE
            activeWorksheet.get_Range("E1").Value2 = "EUR to HUF Currency Exchange Rates From 2015-01-01 to 2020-04-01";

            //*******************************************************************************************GET 2015 DATA
            string xmlQueryResult_2015 = GetExchangeRates("2015-01-01", "2015-12-31", "EUR,HUF");

            //PUT 1 year XML Query in 1 cell - debug mode
            //activeWorksheet.get_Range("A1").Value2 = xmlQueryResult_2015;

            XDocument xdoc_2015 = XDocument.Parse(xmlQueryResult_2015);
            var rates_of_2015 = xdoc_2015.Descendants("Rate");
            var dates_of_2015 = xdoc_2015.Descendants("Day");

            //start Excel Row Counter in order to put data under row by row...
            int excel_cell_row_counter = 3;
            //Put data to cells
            foreach (var dates in dates_of_2015)
            {
                activeWorksheet.get_Range("A" + excel_cell_row_counter.ToString()).Value2 = dates.Attribute("date").Value;
                excel_cell_row_counter++;
            }

            excel_cell_row_counter = 3;
            foreach (var rate in rates_of_2015)
            {
                activeWorksheet.get_Range("B" +excel_cell_row_counter.ToString()).Value2 = float.Parse(rate.Value);
                activeWorksheet.get_Range("B" +excel_cell_row_counter.ToString()).NumberFormat = "$#,##0.00_);($#,##0.00)";
                excel_cell_row_counter++;
            }

            //*******************************************************************************************GET 2016 DATA
            string xmlQueryResult_2016 = GetExchangeRates("2016-01-01", "2016-12-31", "EUR,HUF");
            XDocument xdoc_2016 = XDocument.Parse(xmlQueryResult_2016);
            var rates_of_2016 = xdoc_2016.Descendants("Rate");
            var dates_of_2016 = xdoc_2016.Descendants("Day");

            excel_cell_row_counter = 3;
            //Put data to cells
            foreach (var dates in dates_of_2016)
            {
                activeWorksheet.get_Range("D" + excel_cell_row_counter.ToString()).Value2 = dates.Attribute("date").Value;
                excel_cell_row_counter++;
            }

            excel_cell_row_counter = 3;
            foreach (var rate in rates_of_2016)
            {
                activeWorksheet.get_Range("E" + excel_cell_row_counter.ToString()).Value2 = float.Parse(rate.Value);
                activeWorksheet.get_Range("E" + excel_cell_row_counter.ToString()).NumberFormat = "$#,##0.00_);($#,##0.00)";
                excel_cell_row_counter++;
            }
            //*******************************************************************************************GET 2017 DATA
            string xmlQueryResult_2017 = GetExchangeRates("2017-01-01", "2017-12-31", "EUR,HUF");
            XDocument xdoc_2017 = XDocument.Parse(xmlQueryResult_2017);
            var rates_of_2017 = xdoc_2017.Descendants("Rate");
            var dates_of_2017 = xdoc_2017.Descendants("Day");

            excel_cell_row_counter = 3;
            //Put data to cells
            foreach (var dates in dates_of_2017)
            {
                activeWorksheet.get_Range("G" + excel_cell_row_counter.ToString()).Value2 = dates.Attribute("date").Value;
                excel_cell_row_counter++;
            }

            excel_cell_row_counter = 3;
            foreach (var rate in rates_of_2017)
            {
                activeWorksheet.get_Range("H" + excel_cell_row_counter.ToString()).Value2 = float.Parse(rate.Value);
                activeWorksheet.get_Range("H" + excel_cell_row_counter.ToString()).NumberFormat = "$#,##0.00_);($#,##0.00)";
                excel_cell_row_counter++;
            }
            //*******************************************************************************************GET 2018 DATA
            string xmlQueryResult_2018 = GetExchangeRates("2018-01-01", "2018-12-31", "EUR,HUF");
            XDocument xdoc_2018 = XDocument.Parse(xmlQueryResult_2018);
            var rates_of_2018 = xdoc_2018.Descendants("Rate");
            var dates_of_2018 = xdoc_2018.Descendants("Day");

            excel_cell_row_counter = 3;
            //Put data to cells
            foreach (var dates in dates_of_2018)
            {
                activeWorksheet.get_Range("J" + excel_cell_row_counter.ToString()).Value2 = dates.Attribute("date").Value;
                excel_cell_row_counter++;
            }

            excel_cell_row_counter = 3;
            foreach (var rate in rates_of_2018)
            {
                activeWorksheet.get_Range("K" + excel_cell_row_counter.ToString()).Value2 = float.Parse(rate.Value);
                activeWorksheet.get_Range("K" + excel_cell_row_counter.ToString()).NumberFormat = "$#,##0.00_);($#,##0.00)";
                excel_cell_row_counter++;
            }
            //*******************************************************************************************GET 2019 DATA
            string xmlQueryResult_2019 = GetExchangeRates("2019-01-01", "2019-12-31", "EUR,HUF");
            XDocument xdoc_2019 = XDocument.Parse(xmlQueryResult_2019);
            var rates_of_2019 = xdoc_2019.Descendants("Rate");
            var dates_of_2019 = xdoc_2019.Descendants("Day");

            excel_cell_row_counter = 3;
            //Put data to cells
            foreach (var dates in dates_of_2019)
            {
                activeWorksheet.get_Range("M" + excel_cell_row_counter.ToString()).Value2 = dates.Attribute("date").Value;
                excel_cell_row_counter++;
            }

            excel_cell_row_counter = 3;
            foreach (var rate in rates_of_2019)
            {
                activeWorksheet.get_Range("N" + excel_cell_row_counter.ToString()).Value2 = float.Parse(rate.Value);
                activeWorksheet.get_Range("N" + excel_cell_row_counter.ToString()).NumberFormat = "$#,##0.00_);($#,##0.00)";
                excel_cell_row_counter++;
            }
            //*******************************************************************************************GET 2020 DATA
            string xmlQueryResult_2020 = GetExchangeRates("2020-01-01", "2020-04-01", "EUR,HUF");
            XDocument xdoc_2020 = XDocument.Parse(xmlQueryResult_2020);
            var rates_of_2020 = xdoc_2020.Descendants("Rate");
            var dates_of_2020 = xdoc_2020.Descendants("Day");

            excel_cell_row_counter = 3;
            //Put data to cells
            foreach (var dates in dates_of_2020)
            {
                activeWorksheet.get_Range("P" + excel_cell_row_counter.ToString()).Value2 = dates.Attribute("date").Value;
                excel_cell_row_counter++;
            }

            excel_cell_row_counter = 3;
            foreach (var rate in rates_of_2020)
            {
                activeWorksheet.get_Range("Q" + excel_cell_row_counter.ToString()).Value2 = float.Parse(rate.Value);
                activeWorksheet.get_Range("Q" + excel_cell_row_counter.ToString()).NumberFormat = "$#,##0.00_);($#,##0.00)";
                excel_cell_row_counter++;
            }


            

        } // end of public void MNB_ExchangeRates_VSTO_Excel_AddIn()

    } //end of Class
} //end of Namespace
