# C# VSTO Excel add-in creation  #
### Create a Currency-exchange-rate-history downloader inside MS Excel as an Excel add-in (plugin) in C# VSTO (Ribbon menu button) ###
### Handle the following functions with the press of Ribbon-menu button (Button name: MNB data download): ###
### Create new XLS Sheet with data got from MNB webservice (http://www.mnb.hu/arfolyamok.asmx) ###
### (MNB is Magyar Nemzeti Bank, the First National Bank in Hungary - it's got a public webservice linked above) ###

* First, download the needed data for the following date-interval: 2015.01.01 - 2020.04.01,
* Data need to be formatted as in look, and in the right format, ex.:the numbers in Number format, dates in Date format, etc...,
* Care about running duration, as if possible, program need to be as fast as possible,
* Write Comments on own functions, what it's doing exactly, inputs, outputs...,
* Put the Ribbon-button up from Designer,
* The Button's Group-name is your own name,
* Get the exchange-rates between EUR & HUF.

- After press the Button, create the new XLS Sheet with the data described above.

## Runtime screenshots in SCSHOTS folder ##